from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
from .models import *

def login(request):
	if request.method=='GET':
	    return render(request,'login.html',{})
	else:
		auth = authenticateRationOfficer(request.POST['username'],
		request.POST['password']
		)

		message = ["Unsuccessfull login","User successfully logged in"][auth]
	
		if auth:
			request.session['logged_in'] = True
		return render(request,'login.html',{'message':message})


def loggedin(request):
        return render(request,'ration_officer_home.html',{})

@csrf_exempt
def get_issues(request):
        R_O = RationOfficer.objects.get(Username=request.POST['username'])
        if not R_O:
                return JsonReponse({'status':False})
        print R_O
        complaints = ComplaintLog.objects.filter(Office=R_O.RationOffice)
        json_arr = []
        if complaints:
                json_arr = create_json(complaints)
        return JsonResponse({'status':True,'complaints':json_arr})

def create_json(complaints):
        mylist = list()
        for complaint in complaints:
                obj = dict()
                obj['name'] = complaint.RationId.Username
                obj['shopkeeper'] = complaint.Shop.ShopKeeper
                obj['location'] = complaint.Shop.Location
                obj['description']= complaint.Complaint
                mylist.append(obj)
        return mylist
