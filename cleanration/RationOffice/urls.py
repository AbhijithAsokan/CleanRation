from django.conf.urls import url

from .views import login,loggedin,get_issues

urlpatterns=[
    url('login$', login),
    url('redir$', loggedin),
    url('issues$', get_issues)
]    
